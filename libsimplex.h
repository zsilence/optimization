#pragma once
#include <iostream>
#include <cmath>
#include <limits>
#include <eigen3/Eigen/Dense>


Eigen::ArrayXi simplex_method(Eigen::MatrixXd& A, Eigen::VectorXd& b, Eigen::RowVectorXd& c, Eigen::VectorXd& x);
