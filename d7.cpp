#include <iostream>
#include <cmath>
#include <limits>
#include <vector>
#include <eigen3/Eigen/Dense>
#include "libsimplex.h"

using namespace Eigen;

static double inf = std::numeric_limits<double>::infinity();
static double eps = std::numeric_limits<double>::epsilon() * 1e4;
// double eps = (double)std::numeric_limits<float>::epsilon();


VectorXd lp_special_form(
    const VectorXd& c,
    const MatrixXd& A,
    const VectorXd& b,
    const VectorXd& binf,
    const VectorXd& bsup,
    const std::vector<int> g,
    bool min
) {
    MatrixXd Ax(A.rows() + A.cols(), A.cols());
    Ax <<                                      A,
          MatrixXd::Identity(A.cols(), A.cols());
    MatrixXd Axx(Ax.rows(), Ax.cols() + Ax.rows());
    MatrixXd E = MatrixXd::Identity(Ax.rows(), Ax.rows());
    for(int i = 0; i < g.size(); ++i) {
        E(g[i], g[i]) = -1.;
    }
    Axx << Ax, E;

    VectorXd bx(Ax.rows());
    bx << b, bsup;
    for(int i = 0; i < binf.size(); ++i) {
        bx -= binf[i] * Axx.col(i);
    }
        
    RowVectorXd cx(Axx.cols());
    if(min) {
        cx << -c.transpose(), RowVectorXd::Zero(Axx.rows());
    } else {
        cx << c.transpose(), RowVectorXd::Zero(Axx.rows());        
    }

    VectorXd x(Axx.cols());
    simplex_method(Axx, bx, cx, x);
    x.conservativeResize(c.size());
    for(int i = 0; i < c.size(); ++i) {
        x[i] += binf[i];
    }

    return x;
}

VectorXd convex_programming_problem(
    const VectorXd& c,
    const MatrixXd& D,
    int m,
    const VectorXd cc[],
    const MatrixXd Dc[],
    const double alpha[],
    const VectorXd& xs,
    const VectorXd& xw
) {
    VectorXd x = xw;

    VectorXd gradfxw = c + D * xw;

    MatrixXd A;
    for(int i = 0; i < m; ++i) {
        if(std::abs((cc[i].transpose() * xw + 0.5 * xw.transpose() * Dc[i] * xw)(0) + alpha[i]) <= eps) {
            A = (cc[i] + Dc[i] * xw).transpose();
            for(++i; i < m; ++i) {
                if(std::abs((cc[i].transpose() * xw + 0.5 * xw.transpose() * Dc[i] * xw)(0) + alpha[i]) <= eps) {
                    A.conservativeResize(A.rows() + 1, A.cols());
                    A.bottomRows<1>() = (cc[i] + Dc[i] * xw).transpose();
                }
            }

            break;
        }
    }

    VectorXd binf = VectorXd::Zero(c.size());
    for(int i = 0; i < c.size(); ++i) {
        if(std::abs(xw[i]) >= eps) {
            binf[i] = -1.;
        }
    }

    VectorXd l = lp_special_form(gradfxw, A, VectorXd::Zero(A.rows()), binf, VectorXd::Constant(A.cols(), 1.), std::vector<int>(), true);

    if(std::abs(gradfxw.transpose() * l) <= eps) return x;

    double a = gradfxw.transpose() * l;
    double b = (xs - xw).transpose() * gradfxw;
    double alpha1;
    if(b >= eps) {
        alpha1 = -0.5 * a / b;
    } else {
        alpha1 = 1.;
    }

    for(double t = 1.;; t /= 2.) {
        x = xw + t * l + alpha1 * t * (xs - xw);
        if((x.array() <= -eps).any()) continue;
        bool f = false;
        for(int i = 0; i < m; ++i) {
            if((cc[i].transpose() * x + 0.5 * x.transpose() * Dc[i] * x)(0) + alpha[i] >= eps) {
                f = true;
                break;
            }
        }
        if(f) continue;
        if((c.transpose() * x + 0.5 * x.transpose() * D * x)(0) >= (c.transpose() * xw + 0.5 * xw.transpose() * D * xw)(0) - eps) continue;

        break;
    }

    return x;
}

int main() {
    int m = 5, n = 8;
    VectorXd c(n);
    MatrixXd D(n, n);
    VectorXd cc[m];
    MatrixXd Dc[m];
    double alpha[m];
    VectorXd xs;
    VectorXd xw(n);

    eps = 0.5e-3;

    c << -1., -1., -1., -1., -2., 0., -2., -3.;

    MatrixXd B(n - m, n);

    B << 2., 1., 0., 4., 0., 3., 0., 0.,
         0., 4., 0., 3., 1., 1., 3., 2.,
         1., 3., 0., 5., 0., 4., 0., 4.;

    D = B.transpose() * B;

    for(int i = 0; i < m; ++i) {
        cc[i] = VectorXd(n);
    }

    cc[0] << 0., 60., 80., 0., 0., 0., 40., 0.;
    cc[1] << 2., 0., 3., 0., 2., 0., 3., 0.;
    cc[2] << 0., 0., 80., 0., 0., 0., 0., 0.;
    cc[3] << 0., -2., 1., 2., 0., 0., -2., 1.;
    cc[4] << -4., -2., 6., 0., 4., -2., 60., 2.;

    MatrixXd Bc[m];
    for(int i = 0; i < m; ++i) {
        Bc[i] = MatrixXd(n - m, n);
    }

    Bc[0] << 0., 0., 0.5, 2.5, 1., 0., -2.5, -2.,
             0.5, 0.5, -0.5, 0., 0.5, -0.5, -0.5, -0.5,
             0.5, 0.5, 0.5, 0., 0.5, 1., 2.5, 4.;

    Bc[1] << 1., 2., -1.5, 3., -2.5, 0., -1., -0.5,
             -1.5, -0.5, -1., 2.5, 3.5, 3., -1.5, -0.5,
             1.5, 2.5, 1., 1., 2.5, 1.5, 3., 0.;

    Bc[2] << 0.75, 0.5, -1., 0.25, 0.25, 0., 0.25, 0.75,
             -1., 1., 1., 0.75, 0.75, 0.5, 1., -0.75,
             0.5, -0.25, 0.5, 0.75, 0.5, 1.25, -0.75, -0.25;

    Bc[3] << 1.5, -1.5, -1.5, 2., 1.5, 0., 0.5, -1.5,
             -0.5, -2.5, -0.5, -1., -2.5, 2.5, 1., 2.,
             -2.5, 1., -2., -1.5, -2.5, 0.5, 2.5, -2.5;

    Bc[4] << 1., 0.25, -0.5, 1.25, 1.25, -0.5, 0.25, -0.75,
             -1., -0.75, -0.75, 0.5, -0.25, 1.25, 0.25, -0.5,
             0., 0.75, 0.5, -0.5, -1., 1., -1., 1.;
    
    for(int i = 0; i < m; ++i) {
        Dc[i] = Bc[i].transpose() * Bc[i];
    }

    alpha[0] = -51.75;
    alpha[1] = -436.75;
    alpha[2] = -33.7813;
    alpha[3] = -303.375;
    alpha[4] = -41.75;

    xs = VectorXd::Zero(n);

    xw << 1., 0., 0., 2., 4., 2., 0., 0.;

    VectorXd x = convex_programming_problem(c, D, m, cc, Dc, alpha, xs, xw);

    std::cout << "x: " << x.transpose() << std::endl;

    std::cout << "f: " << c.transpose() * x + 0.5 * x.transpose() * D * x << std::endl;

    return 0;
}
