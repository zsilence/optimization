#include <iostream>
#include <cmath>
#include <limits>
#include <eigen/Eigen/Dense>

using namespace Eigen;

double inf = std::numeric_limits<double>::infinity();
double eps = std::numeric_limits<double>::epsilon() * 1e4;


MatrixXd inverse_matrix(const MatrixXd& a_1, const VectorXd& x, const int i) {
    VectorXd l = a_1 * x;

    if(std::abs(l[i]) <= eps){
        std::cout << "no inverse matrix." << std::endl;
        exit(0);
    }

    double li = -l[i];
    l[i] = -1.;
    l /= li;

    MatrixXd A_1 = MatrixXd::Identity(l.rows(), l.rows());
    A_1.col(i) = l;

    return (A_1 * a_1);
}

MatrixXd main_phase(const MatrixXd& A, const RowVectorXd& c, VectorXd& x, ArrayXi& Jb) {
    int m = A.rows(), n = A.cols();
    MatrixXd Ab(m, m);
    RowVectorXd cb(m);
    for(int i = 0; i < m; ++i) {
        Ab.col(i) = A.col(Jb[i]);
        cb[i] = c[Jb[i]];
    }
    MatrixXd Ab_1 = Ab.inverse();
    
    RowVectorXd u, delta;
    VectorXd z;
    for(;;) {
        u = cb * Ab_1;
        delta = u * A - c;

        if((delta.array() >= -eps).all()) return Ab_1;

        int j0;
        for(int i = 0; i < n; ++i) {
            if(delta[i] <= -eps) {
                j0 = i;
                break;
            }
        }

        z = Ab_1 * A.col(j0);

        if((z.array() <= eps).all()) {
            std::cout << "f is unbounded." << std::endl;
            exit(0);
        }

        int s;
        double theta0 = inf, theta;
        for(int i = 0, j = n - 1; i < m; ++i) {
            z[i] <= eps ? theta = inf : theta = x[Jb[i]] / z[i];
            if(theta <= theta0 - eps) {
                j = Jb[i];
                theta0 = theta;
                s = i;
            } else if((std::abs(theta - theta0) <= eps) && (Jb[i] <= j)) {
                j = Jb[i];
                theta0 = std::min(theta, theta0);
                s = i;
            }
        }

        x[Jb[s]] = 0.;
        Jb[s] = j0;
        for(int i = 0; i < m; ++i) {
            x[Jb[i]] -= theta0 * z[i];
        }
        x[j0] = theta0;

        Ab_1 = inverse_matrix(Ab_1, A.col(j0), s);

        for(int i = 0; i < m; ++i) {
            cb[i] = c[Jb[i]];
        }

    }

    return Ab_1;
}

ArrayXi first_phase(MatrixXd& A, VectorXd& b, RowVectorXd& c, VectorXd& x) {
    int m = A.rows(), n = A.cols();

    for(int i = 0; i < m; ++i) {
        if(b[i] <= -eps) {
            b[i] = -b[i];
            A.row(i) = -A.row(i);
        } else if(std::abs(b[i]) <= eps) {
            b[i] = 0.;
        }
    }

    ArrayXi Jb;
    MatrixXd Ab_1;
    VectorXd xx;
    RowVectorXd cx;
    MatrixXd Ax;
    for(;;) {
        xx.resize(n + m);
        xx << VectorXd::Zero(n), b;
        cx.resize(n + m);
        cx << RowVectorXd::Zero(n), RowVectorXd::Constant(m, -1.);
        Ax.resize(m, n + m);
        Ax << A, MatrixXd::Identity(m, m);
        Jb = ArrayXi::LinSpaced(m, n, n + m - 1);
        Ab_1 = main_phase(Ax, cx, xx, Jb);

        if((xx.tail(m).array().abs() >= eps).any()) {
            std::cout << "no valid plans." << std::endl;
            exit(0);
        }

        x = xx.head(n);

        if((Jb < n).all()) break;

        int k;
        for(k = 0; k < m; ++k) {
            if(Jb[k] < n) continue;

            for(int j = 0; j < n; ++j) {
                if((Jb == j).any()) continue;
                
                VectorXd l = Ab_1 * A.col(j);
                if(std::abs(l[k]) >= eps) {
                    Jb[k] = j;
                    break;
                }
            }

            if(Jb[k] >= n) break;
        }

        if(k == m) break;

        int i = Jb[k] - n;
        A.block(i, 0, m - 1 - i, n) = A.block(i + 1, 0, m - 1 - i, n);
        b.block(i, 0, m - 1 - i, 1) = b.block(i + 1, 0, m - 1 - i, 1);
        --m;
        A.conservativeResize(m, n);
        b.conservativeResize(m);
    }

    return Jb;
}

ArrayXi simplex_method(MatrixXd& A, VectorXd& b, RowVectorXd& c, VectorXd& x) {
    ArrayXi Jb = first_phase(A, b, c, x);
    main_phase(A, c, x, Jb);
    return Jb;
}

int main() {
    MatrixXd A(4, 5);
    VectorXd b(4);
    RowVectorXd c(5);
    VectorXd x(5);

    A << -1., 1., 1., 0., 0.,
          1., 0., 0., 1., 0.,
          2., 0., 0., 2., 0.,
          0., 1., 0., 0., 1.;

    b << 1.,
         3.,
         6.,
         2.;

    c << 1., 1., 0., 0., 0.;

    ArrayXi Jb = simplex_method(A, b, c, x);

    std::cout << "Jb: " << std::endl << Jb << std::endl << std::endl;
    std::cout << "x: " << std::endl << x << std::endl << std::endl;
    std::cout << "c'x: " << (c * x) << std::endl << std::endl;

    return 0;
}