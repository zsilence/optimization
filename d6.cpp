#include <iostream>
#include <cmath>
#include <limits>
#include <eigen3/Eigen/Dense>

using namespace Eigen;

double inf = std::numeric_limits<double>::infinity();
double eps = std::numeric_limits<double>::epsilon() * 1e4;
// double eps = (double)std::numeric_limits<float>::epsilon();


MatrixXd inverse_matrix(const MatrixXd& a_1, const VectorXd& x, const int i) {
    VectorXd l = a_1 * x;

    if(std::abs(l[i]) <= eps){
        std::cout << "no inverse matrix." << std::endl;
        exit(0);
    }

    double li = -l[i];
    l[i] = -1.;
    l /= li;

    MatrixXd A_1 = MatrixXd::Identity(l.rows(), l.rows());
    A_1.col(i) = l;

    return (A_1 * a_1);
}

void quadratic_programming_problem(MatrixXd& A, VectorXd& b, RowVectorXd& c, MatrixXd& D, VectorXd& x, ArrayXi& Js, ArrayXi& Jse) {
    int m = A.rows(), n = A.cols();

    MatrixXd As(m, m);
    for(int i = 0; i < m; ++i) {
        As.col(i) = A.col(Js[i]);
    }
    MatrixXd As_1 = As.inverse();

    int esize = Jse.size();
    MatrixXd Ase(m, esize);
    for(int i = 0; i < esize; ++i) {
        Ase.col(i) = A.col(Jse[i]);
    }

    RowVectorXd cxs(m);
    RowVectorXd cx;
    RowVectorXd ux;
    RowVectorXd deltax;
    MatrixXd De;
    MatrixXd H;
    VectorXd bs;
    VectorXd bs1;
    VectorXd xs;
    VectorXd l;
    bool shorty = false;
    int j0;
    double theta0, delta;
    for(int g = 1;; g++) {
        if(!shorty) {
            cx = c + x.transpose() * D;
            for(int i = 0; i < m; ++i) {
                cxs[i] = cx[Js[i]];
            }
            ux = -cxs * As_1;
            deltax = ux * A + cx;

            if((deltax.array() >= -eps).all()) break;

            for(int i = 0; i < n; ++i) {
                if(deltax[i] <= -eps) {
                    j0 = i;
                    break;
                }
            }
        } else {
            deltax[j0] += theta0 * delta;
        }

        shorty = false;

        De = MatrixXd(esize, esize);
        for(int i = 0, k = 0; i < esize; ++i) {
            for(int j = 0; j < esize; ++j) {
                De(i, j) = D(Jse[i], Jse[j]);
            }
        }

        H.conservativeResize(esize + m, esize + m);
        H <<  De,      Ase.transpose(),
             Ase, MatrixXd::Zero(m, m);

        bs.conservativeResize(esize + m);
        bs1.conservativeResize(esize);
        for(int i = 0; i < esize; ++i) {
            bs1[i] = D(Jse[i], j0);
        }
        bs << -bs1, -A.col(j0);

        xs = H.inverse() * bs;

        l = VectorXd::Zero(n);
        l[j0] = 1.;
        for(int i = 0; i < esize; ++i) {
            l[Jse[i]] = xs[i];
        }

        delta = l.transpose() * D * l;
        double thetaj = delta >= eps ? std::abs(deltax[j0]) / delta : inf;
        double theta;
        theta0 = inf;
        int s;
        for(int i = 0; i < esize; ++i) {
            theta = l[Jse[i]] <= -eps ? -x[Jse[i]] / l[Jse[i]] : inf;
            if(theta <= theta0 - eps || (std::abs(theta - theta0) <= eps && Jse[i] < Jse[s])) {
                theta0 = theta;
                s = i;
            }
        }
        if(thetaj <= theta0 - eps || (std::abs(thetaj - theta0) <= eps && j0 < Jse[s])) {
            theta0 = thetaj;
            s = -1;
        }

        if(theta0 >= inf - eps) {
            std::cout << "f is infeasible." << std::endl;
            exit(0);
        }

        x += theta0 * l;

        if(s == -1) {
            Jse.conservativeResize(esize + 1);
            Ase.conservativeResize(m, esize + 1);
            Ase.col(esize) = A.col(j0);
            Jse[esize++] = j0;
        } else {
            int ss = -1;
            for(int i = 0; i < m; ++i) {
                if(Js[i] == Jse[s]) {
                    ss = i;
                    break;
                }
            }

            if(ss == -1) {
                shorty = true;
                Jse.segment(s, esize - 1 - s) = Jse.tail(esize - 1 - s);
                Ase.block(0, s, m, esize - 1 - s) = Ase.block(0, s + 1, m, esize - 1 - s);
                Jse.conservativeResize(--esize);
                Ase.conservativeResize(m, esize);
            } else {
                int sp = -1;
                for(int i = 0; i < esize; ++i) {
                    if((Js != Jse[i]).all() && std::abs((As_1 * A.col(Jse[i]))[ss]) >= eps) {
                        sp = i;
                        break;
                    }
                }

                if(sp == -1) {
                    Js[ss] = j0;
                    Jse[s] = j0;
                    Ase.col(s) = A.col(j0);
                } else {
                    shorty = true;
                    Js[ss] = Jse[sp];
                    Jse.segment(s, esize - 1 - s) = Jse.tail(esize - 1 - s);
                    Ase.block(0, s, m, esize - 1 - s) = Ase.block(0, s + 1, m, esize - 1 - s);
                    Jse.conservativeResize(--esize);
                    Ase.conservativeResize(m, esize);
                }

                As_1 = inverse_matrix(As_1, A.col(Js[ss]), ss);
            }
        }
    }

    return;
}

int main() {
    MatrixXd A(3, 8);
    VectorXd b(3);
    RowVectorXd c(8);
    MatrixXd D(8, 8);
    VectorXd x(8);
    ArrayXi Js(3);
    ArrayXi Jse(3);

    MatrixXd B(3, 8);

    B << 1., 0., 0., 3., -1., 5., 0., 1.,
         2., 5., 0., 0., 0., 4., 0., 0.,
         -1., 9., 0., 5., 2., -1., -1., 5.;

    c << -13., -217., 0., -117., -27., -71., 18., -99.;

    D = B.transpose() * B;

    A << 2., -3., 1., 1., 3., 0., 1., 2.,
         -1., 3., 1., 0., 1., 4., 5., -6.,
         1., 1., -1., 0., 1., -2., 4., 8.;

    b << 8., 4., 14.;

    x << 0., 2., 0., 0., 4., 0., 0., 1.;

    Jse << 1, 4, 7;

    Js << Jse;

    quadratic_programming_problem(A, b, c, D, x, Js, Jse);

    std::cout << "x: " << x.transpose() << std::endl;

    std::cout << "f: " << c * x + 0.5 * x.transpose() * D * x << std::endl;

    std::cout << "is valid: " << (((A * x - b).array().abs() <= eps).all() && (x.array() >= -eps).all()) << std::endl;

    return 0;
}