#include <iostream>
#include <cmath>
#include <queue>
#include <vector>
#include <unordered_map>
#include <stack>
#include <algorithm>
#include <eigen3/Eigen/Dense>

using namespace Eigen;


void print_base(const std::vector<std::vector<int>>& Jb) {
    for(std::vector<std::vector<int>>::const_iterator it = Jb.cbegin(); it != Jb.cend(); ++it) {
        std::cout << it - Jb.cbegin() << " : ";
        for(std::vector<int>::const_iterator iter = it->cbegin(); iter != it->cend(); ++iter) {
            std::cout << *iter << " ";
        }
        std::cout << std::endl;
    }
}

void add_edge(std::vector<std::vector<int>>& Jb, int u, int v) {
    Jb[u].emplace_back(v);
    Jb[v].emplace_back(u);
}

void delete_edge(std::vector<std::vector<int>>& Jb, int u, int v) {
    Jb[u].erase(std::remove(Jb[u].begin(), Jb[u].end(), v), Jb[u].end());
    Jb[v].erase(std::remove(Jb[v].begin(), Jb[v].end(), u), Jb[v].end());
}

std::vector<std::vector<int>> northwest_corner_method(VectorXi a, RowVectorXi b, MatrixXi& X) {
    int m = a.size(), n = b.size();
    int size = m + n - 2;
    std::vector<std::vector<int>> Jb(m * n, std::vector<int>());

    int i = 0, j = 0, first, second;
    while(size--) {
        first = i * n + j;
        if(a[i] <= b[j]) {
            b[j] -= a[i];
            X(i, j) = a[i];
            ++i;
        } else {
            a[i] -= b[j];
            X(i, j) = b[j];
            ++j;
        }
        second = i * n + j;

        add_edge(Jb, first, second);
    }
    X(i, j) = a[i];

    return Jb;
}

void solve(const MatrixXi& c, const std::vector<std::vector<int>>& Jb, ArrayXi& u, ArrayXi& v, int start) {
    int n = c.cols();
    int i = start / n;
    int j = start % n;
    
    u[i] = 0;
    v[j] = c(i, j);
    std::queue<std::pair<int, int>> q;
    std::vector<int>::const_iterator it;
    for(it = Jb[start].cbegin(); it != Jb[start].cend(); ++ it) {
        q.emplace(start, *it);
    }
    std::pair<int, int> to;
    while(!q.empty()) {
        to = q.front();
        q.pop();
        
        i = to.second / n;
        j = to.second % n;
        if((to.first / n) == i) {
            v[j] = c(i, j) - u[i];
        } else {
            u[i] = c(i, j) - v[j];
        }

        for(it = Jb[to.second].begin(); it != Jb[to.second].end(); ++it) {
            if(*it != to.first) {
                q.emplace(to.second, *it);
            }
        }
    }

    return;
}

int expand_base(const MatrixXi& c, const ArrayXi& u, const ArrayXi& v, std::vector<std::vector<int>>& Jb) {
    int m = c.rows(), n = c.cols(), delta = 0, s;
    for(int i = 0, tmp; i < m; ++i) {
        for(int j = 0; j < n; ++j) {
            if((tmp = c(i, j) - u[i] - v[j]) < delta) {
                delta = tmp;
                s = i * n + j;
            }
        }
    }

    if(delta == 0) return -1;

    int max = std::max(m, n);
    std::vector<int> b(4, -1);
    for(int diff = 1, i = s / n, j = s % n; diff < max; ++diff) {
        if(b[0] == -1 && (i + diff) < m && !Jb[(i + diff) * n + j].empty()) b[0] = (i + diff) * n + j;
        if(b[1] == -1 && (i - diff) > -1 && !Jb[(i - diff) * n + j].empty()) b[1] = (i - diff) * n + j;
        if(b[2] == -1 && (j + diff) < n && !Jb[s + diff].empty()) b[2] = s + diff;
        if(b[3] == -1 && (j - diff) > -1 && !Jb[s - diff].empty()) b[3] = s - diff;
    }

    if(b[0] != -1 && b[1] != -1 && b[0] % n == b[1] % n) {
        delete_edge(Jb, b[0], b[1]);
    }
    if(b[2] != -1 && b[3] != -1 && b[2] / n == b[3] / n) {
        delete_edge(Jb, b[2], b[3]);        
    }

    for(int i = 0; i < 4; ++i) {
        if(b[i] != -1) {
            add_edge(Jb, s, b[i]);
        }
    }
    
    return s;
}

std::vector<int> find_cycle(const std::vector<std::vector<int>>& Jb, int n, int start) {
    std::vector<int> cycle;
    std::stack<int> st;
    std::unordered_map<int, int> from;
    std::unordered_map<int, bool> leave;

    st.emplace(start);
    from[start] = -1;

    int p;
    std::vector<int>::const_iterator it;
    while(!st.empty()) {
        p = st.top();

        for(it = Jb[p].begin(); it != Jb[p].end(); ++it) {
            if(!leave[*it] && from[*it] && from[p] != (*it + 1)) {
                cycle.emplace_back(*it);
                int next, current, prev = *it;
                while(*it != st.top()) {
                    current = st.top();
                    st.pop();
                    next = st.top();
                    if(prev / n != next / n && prev % n != next % n) {
                        cycle.emplace_back(current);
                    }
                    prev = current;
                }
                
                while(!st.empty()) st.pop();
                
                break;
            }

            if(from[*it]) continue;            

            st.emplace(*it);
            from[st.top()] = p + 1;
            break;
        }

        if(!st.empty() && st.top() == p) {
            leave[p] = true;
            st.pop();
        }
    }

    return cycle;
}

void rebuild_base(MatrixXi& X, std::vector<std::vector<int>>& Jb, const std::vector<int>& cycle) {
    int n = X.cols();
    int ss = cycle[1];
    int i = ss / n, j = ss % n;
    int theta = X(i, j);
    for(int k = 1; k < cycle.size(); k += 2) {
        i = cycle[k] / n;
        j = cycle[k] % n;
        if(X(i, j) < theta) {
            theta = X(i, j);
            ss = cycle[k];
        }
    }

    for(i = 0; i < cycle.size(); ++i) {
        if(i % 2) {
            X(cycle[i] / n, cycle[i] % n) -= theta;
        } else {
            X(cycle[i] / n, cycle[i] % n) += theta;
        }
    }

    for(i = 0; i < Jb[ss].size(); ++i) {
        Jb[Jb[ss][i]].erase(std::remove(Jb[Jb[ss][i]].begin(), Jb[Jb[ss][i]].end(), ss), Jb[Jb[ss][i]].end());
    }

    for(i = 0; i < Jb[ss].size() - 1; ++i) {
        for(j = i + 1; j < Jb[ss].size(); ++j) {
            if((Jb[ss][i] / n) == (Jb[ss][j] / n) || (Jb[ss][i] % n) == (Jb[ss][j] % n)) {
                add_edge(Jb, Jb[ss][i], Jb[ss][j]);
            }
        }
    }

    Jb[ss].clear();

    return;
}

void potential_method(const MatrixXi& c, MatrixXi& X, std::vector<std::vector<int>>& Jb) {
    ArrayXi u(c.rows()), v(c.cols());
    int s = 0;
    std::vector<int> cycle;
    for(;;) {
        solve(c, Jb, u, v, s);

        s = expand_base(c, u, v, Jb);

        if(s == -1) return;

        cycle = find_cycle(Jb, c.cols(), s);

        rebuild_base(X, Jb, cycle);
    }

    return;
}

MatrixXi matrix_transportation_problem(VectorXi& a, RowVectorXi& b, MatrixXi& c) {
    int n = b.size();
    int m = a.size();

    int sa = a.sum();
    int sb = b.sum();
    
    if(sa > sb) {
        b.conservativeResize(n + 1);
        b[n] = sa - sb;
        c.conservativeResize(m, n + 1);
        c.rightCols<1>() = VectorXi::Zero(m);
        ++n;
    } else if(sb > sa) {
        a.conservativeResize(m + 1);
        a[m] = sb - sa;
        c.conservativeResize(m + 1, n);
        c.bottomRows<1>() = RowVectorXi::Zero(n);
        ++m;
    }

    MatrixXi X = MatrixXi::Zero(m, n);
    std::vector<std::vector<int>> Jb = northwest_corner_method(a, b, X);
    potential_method(c, X, Jb);
    
    return X;
}

int main() {
    VectorXi a(4);
    RowVectorXi b(8);
    MatrixXi c(4, 8);

    a << 20, 11, 18, 27;

    b << 11, 4, 10, 12, 8, 9, 10, 4;

    c << -3, 6,  7, 12, 6, -3,  2, 16,
          4, 3,  7, 10, 0,  1, -3,  7,
         19, 3,  2,  7, 3,  7,  8, 15,
          1, 4, -7, -3, 9, 13, 17, 22;

    MatrixXi X = matrix_transportation_problem(a, b, c);
    
    std::cout << "transportation plan: " << std::endl << X << std::endl << std::endl;

    std::cout << "totally: " << (X.array() * c.array()).sum() << std::endl;

    return 0;
}