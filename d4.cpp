#include <iostream>
#include <cmath>
#include <limits>
#include <eigen/Eigen/Dense>

using namespace Eigen;

double inf = std::numeric_limits<double>::infinity();
double eps = std::numeric_limits<double>::epsilon() * 1e4;


MatrixXd inverse_matrix(const MatrixXd& a_1, const VectorXd& x, const int i) {
    VectorXd l = a_1 * x;

    if(std::abs(l[i]) <= eps){
        std::cout << "no inverse matrix." << std::endl;
        exit(0);
    }

    double li = -l[i];
    l[i] = -1.;
    l /= li;

    MatrixXd A_1 = MatrixXd::Identity(l.rows(), l.rows());
    A_1.col(i) = l;

    return (A_1 * a_1);
}

VectorXd dual_simplex_method(const RowVectorXd& c, const MatrixXd& A, const VectorXd& b, ArrayXi& Jb) {
    int m = A.rows(), n = A.cols();
    MatrixXd Ab(m, m);
    RowVectorXd cb(m);
    for(int i = 0; i < m; ++i) {
        Ab.col(i) = A.col(Jb[i]);
        cb[i] = c[Jb[i]];
    }
    MatrixXd Ab_1 = Ab.inverse();

    RowVectorXd y = cb * Ab_1;
    VectorXd chi;
    VectorXd x;
    ArrayXd mu(n);
    for(;;) {
        x = VectorXd::Zero(n);
        chi = Ab_1 * b;
        for(int i = 0; i < m; ++i) {
            x[Jb[i]] = chi[i];
        }
        if((chi.array() >= -eps).all()) return x;

        int s;
        for(int i = 0, j = n - 1; i < m; ++i) {
            if((x[Jb[i]] <= -eps) && (Jb[i] <= j)) {
                j = Jb[i];
                s = i;
            }
        }

        bool f = false;
        for(int i = 0; i < n; ++i) {
            mu[i] = Ab_1.row(s) * A.col(i);
            if(!f) f = (mu[i] <= -eps);
        }

        if(!f) {
            std::cout << "task is infeasible." << std::endl;
            exit(0);
        }

        int j0 = n - 1;
        double sigma0 = inf, sigma;
        for(int i = 0; i < n; ++i) {
            if(mu[i] >= -eps) continue;

            sigma = (c[i] - y * A.col(i)) / mu[i];
            if(sigma <= sigma0 - eps) {
                sigma0 = sigma;
                j0 = i;
            } else if((std::abs(sigma - sigma0) <= eps) && (i <= j0)) {
                sigma0 = std::min(sigma, sigma0);
                j0 = i;
            }
        }

        y += sigma0 * Ab_1.row(s);

        Jb[s] = j0;
        
        Ab_1 = inverse_matrix(Ab_1, A.col(j0), s);
    }

    return x;
}

int main() {
    MatrixXd A(2, 5);
    VectorXd b(2);
    RowVectorXd c(5);
    VectorXd x(5);
    ArrayXi Jb(2);

    A << -2., -1., -4., 1., 0.,
         -2., -2., -2., 0., 1.;

    b << -1.,
         -1.5;

    c << -4., -3., -7., 0., 0.;

    Jb << 3, 4;

    x = dual_simplex_method(c, A, b, Jb);

    std::cout << "Jb: " << std::endl << Jb << std::endl << std::endl;
    std::cout << "x: " << std::endl << x << std::endl << std::endl;
    std::cout << "c'x: " << (c * x) << std::endl << std::endl;

    return 0;
}